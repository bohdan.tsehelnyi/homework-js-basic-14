// ===========================THEORY=======================================
/*
1. В чому полягає відмінність localStorage і sessionStorage?
Відмінність в тому, що sessionStorage зберігає дані до закриття сторінки, після закриття сторінки дані зникають зі сховища. localStorage має більш обширний функціонал і зберігає дані після перезагрузки, закриття/відкриття сторінки, головне щоб була та сама локальна адреса сторінки.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
Коли зберігаєш дані в sessionStorage то потрібно розуміти що вони видаляться після закриття/відкриття браузеа. localStorage зберігає дані доки ти сам їх не видалищ, але дані зберігаються локально на локальній адресі де ти їх зберіг, вони не мають можливості надсилатися на сервер, а також при зміні локальної адреси (домен/протокол/порт) ти можеш їх втратити.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Вони видаляються.sessionStorage зберігає лише дані до закриття браузера, далі вони видаляються.
*/
// =============================PRACTICE===================================
const header = document.querySelector('.container');
const products = document.querySelector('.products');
let btn = document.createElement('div');
btn.setAttribute('class', "hello")
btn.textContent = "Змінити тему";

header.prepend(btn);

btn.style.cssText = "border: 2px solid #fff; height: 30px; width: 150px; color: #000; border-radius: 10px; text-align: center; line-height: 25px; cursor: pointer;"



const myClass = 'dark';
window.onload = function (){
	if(localStorage.getItem('color') !== null){
		let color = localStorage.getItem('color');
		for(let el of products.children){
			el.classList.toggle(color)
		}
	}
btn.addEventListener('click', function(event){
	const data = event.target;
	if(data == btn){
		data.classList.toggle('on');
		for(let el of products.children){
		el.classList.add('dark');
		localStorage.setItem('color', 'dark');
		}
	}if(!data.classList.contains('on')){
		for(let el of products.children){
		el.classList.remove('dark');
		localStorage.removeItem('color', 'dark');
		}
	}
})}; 

